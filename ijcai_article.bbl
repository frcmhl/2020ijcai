\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Bergstra and
  Bengio}{2012}]{bergstra2012random}
James Bergstra and Yoshua Bengio.
\newblock Random search for hyper-parameter optimization.
\newblock {\em Journal of Machine Learning Research}, 13(Feb):281--305, 2012.

\bibitem[\protect\citeauthoryear{Bergstra \bgroup \em et al.\egroup
  }{2011}]{bergstra2011algorithms}
James~S Bergstra, R{\'e}mi Bardenet, Yoshua Bengio, and Bal{\'a}zs K{\'e}gl.
\newblock Algorithms for hyper-parameter optimization.
\newblock In {\em Advances in neural information processing systems}, pages
  2546--2554, 2011.

\bibitem[\protect\citeauthoryear{Commons}{2019}]{wiki:gpbayesian}
Wikimedia Commons.
\newblock File:gpparbayesanimationsmall.gif --- wikimedia commons, the free
  media repository, 2019.
\newblock Online; accessed 16-January-2020.

\bibitem[\protect\citeauthoryear{Dewancker \bgroup \em et al.\egroup
  }{2015}]{dewancker2015bayesian}
Ian Dewancker, Michael McCourt, and Scott Clark.
\newblock Bayesian optimization primer, 2015.

\bibitem[\protect\citeauthoryear{Frazier}{2018}]{frazier2018tutorial}
Peter~I Frazier.
\newblock A tutorial on bayesian optimization.
\newblock {\em arXiv preprint arXiv:1807.02811}, 2018.

\bibitem[\protect\citeauthoryear{Ioffe and
  Szegedy}{2015}]{DBLP:journals/corr/IoffeS15}
Sergey Ioffe and Christian Szegedy.
\newblock Batch normalization: Accelerating deep network training by reducing
  internal covariate shift.
\newblock {\em CoRR}, abs/1502.03167, 2015.

\bibitem[\protect\citeauthoryear{Jalali \bgroup \em et al.\egroup
  }{2012}]{DBLP:journals/corr/abs-1204-0047}
Ali Jalali, Javad Azimi, and Xiaoli~Z. Fern.
\newblock Exploration vs exploitation in bayesian optimization.
\newblock {\em CoRR}, abs/1204.0047, 2012.

\bibitem[\protect\citeauthoryear{Jones \bgroup \em et al.\egroup
  }{1998}]{jones1998efficient}
Donald~R Jones, Matthias Schonlau, and William~J Welch.
\newblock Efficient global optimization of expensive black-box functions.
\newblock {\em Journal of Global optimization}, 13(4):455--492, 1998.

\bibitem[\protect\citeauthoryear{Korichi \bgroup \em et al.\egroup
  }{2019}]{cosmetic}
Korichi, Mathilde Guillemot, Catherine Heusèle, and Rodolphe.
\newblock Tuning neural network hyperparameters through bayesian optimization
  and application to cosmetic formulation data.
\newblock 2019.

\bibitem[\protect\citeauthoryear{Montavon \bgroup \em et al.\egroup
  }{2012}]{montavon2012neural}
Gr{\'e}goire Montavon, Genevi{\`e}ve Orr, and Klaus-Robert M{\"u}ller.
\newblock {\em Neural networks: tricks of the trade}, volume 7700.
\newblock springer, 2012.

\bibitem[\protect\citeauthoryear{Rasmussen}{2003}]{rasmussen2003gaussian}
Carl~Edward Rasmussen.
\newblock Gaussian processes in machine learning.
\newblock In {\em Summer School on Machine Learning}, pages 63--71. Springer,
  2003.

\bibitem[\protect\citeauthoryear{Snoek \bgroup \em et al.\egroup
  }{2012}]{snoek2012practical}
Jasper Snoek, Hugo Larochelle, and Ryan~P Adams.
\newblock Practical bayesian optimization of machine learning algorithms.
\newblock In {\em Advances in neural information processing systems}, pages
  2951--2959, 2012.

\bibitem[\protect\citeauthoryear{van
  Laarhoven}{2017}]{DBLP:journals/corr/Laarhoven17b}
Twan van Laarhoven.
\newblock {L2} regularization versus batch and weight normalization.
\newblock {\em CoRR}, abs/1706.05350, 2017.

\end{thebibliography}
