 %%%% ijcai20.tex

\typeout{IJCAI--PRICAI--20 Instructions for Authors}

% These are the instructions for authors for IJCAI-20.

\documentclass{article}
\pdfpagewidth=8.5in
\pdfpageheight=11in
% The file ijcai20.sty is NOT the same than previous years'
\usepackage{ijcai20}
  
% Use the postscript times font!
\usepackage{times}
\usepackage{soul}
\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage[utf8]{inputenc}
\usepackage[small]{caption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{booktabs}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{lipsum}
\urlstyle{same}

% the following package is optional:
\usepackage{latexsym} 
\newtheorem{example}{Example}
\newtheorem{theorem}{Theorem}
\usepackage{placeins}
\setcounter{secnumdepth}{4}


\title{Automatic Setting of DNN Hyper-Parameters by Mixing Bayesian optimization and Tuning Rules}

\author{
	Michele Fraccaroli$^{1}$\and
	Evelina Lamma$^{1}$\and
	Fabrizio Riguzzi$^{2}$
	\affiliations
	$^{1}$DE - Department of Engineering\\
	$^{2}$DMI - Department of Mathematics and Computer Science \\
	University of Ferrara\\ 
	Via Saragat 1, 44122 Ferrara, Italy
	\emails
	\{michele.fraccaroli, evelina.lamma, fabrizio.riguzzi\}@unife.it
}

\begin{document}

\maketitle

\begin{abstract}
Deep learning techniques play an increasingly important role in industrial and research environments due to their outstanding results. However, the large number of hyper-parameters to set may lead to errors if they are set manually. The state-of-the-art hyper-parameters tuning methods are: grid search, random search  and Bayesian Optimization. The first two methods are expensive because they try, respectively, all possible combinations and random combinations of hyper-parameters without any logic in the selection of values. Bayesian Optimization, instead, builds a surrogate model of the objective function, quantifies the uncertainty in the surrogate using Gaussian Process Regression and uses an acquisition function to decide where to sample the new set of hyper-parameters. This work is placed in the field of Hyper-Parameters Optimization (HPO). The aim is to improve Bayesian Optimization applied to Deep Neural Networks. For this goal, we build a new algorithm for evaluating and analysing the results of the network on the training and validation sets, and use a set of rules to add new hyper-parameters and/or to reduce the hyper-parameter search space in order to select a good combination.
\end{abstract}

\section{Introduction}
\label{intro}
Deep Neural Networks (DNNs) provides outstanding results in many fields but, unfortunately, they are also very sensitive to the tuning of their hyper-parameters. Automatic hyper-parameters optimization algorithms have recently shown good performance and, in some cases, results comparable with human experts \cite{bergstra2011algorithms}.
Tuning a big DNN is computationally expensive and some experts still perform manual tuning of the hyper-parameters. To do this, one can refer to some tricks \cite{montavon2012neural} to determine the best set of hyper-parameter values to use for obtaining a good performance from the network.
The aim of this work is to combine the automatic approach with these tricks used in the manual approach, in a fully-automated integration.
For the automatic approach,  we use a Bayesian Optimization \cite{dewancker2015bayesian} because it limits the evaluations of the objective function (training and validation of the neural network in our case) by spending more time in choosing the next set of hyper-parameter values to try.
We aim at improving Bayesian Optimization \cite{dewancker2015bayesian} by integrating it with a set of tuning rules. These rules have the purpose of reducing the hyper-parameter space or adding new hyper-parameters to set. In this way, we constrain the Bayesian approach to select the hyper-parameters in a restricted space, and add new parameters without human intervention. In this way we can avoid typical problems of the neural networks like overfitting and underfitting, and automatically drive the learning process to good results.

After discussing related work (Section \ref{rw}), we introduce the approach used to analyse the behaviour of the networks, the execution pipeline, the performed analysis and the tuning rules used to fix the detected problems (Section \ref{nba}, Section \ref{EP}, Section \ref{PA} and Section \ref{tr}). Experimental results with different networks and different datasets are described in Section \ref{ex}. All experiments are performed on benchmark datasets, MNIST and CIFAR-10 in particular.

\section{Related Work}
\label{rw}
In this section, we shortly review the state-of-the-art of hyper-parameter optimization for DNNs, i.e., grid search, random search  and, with special attention, Bayesian Optimization.

\subsection{Grid Search}
\label{gs}
Grid Search applies an exhaustive research (also called \textit{brute-force search}) through a manually specified hyper-parameter space. This search is guided by some specified performance metric. 

Grid Search tests the learning algorithm with every combination of the hyper-parameters and, thanks to its \textit{brute-force search}, guarantees to find the optimal configuration. But, of course, this method suffers from the \textit{curse of dimensionality}.
The problem of Grid Search applied to DNNs is that we have a huge amount of hyper-parameters to set and the evaluating phase of the algorithm is very expensive. This definitively raises a time problem.

\subsection{Random Search}
\label{rs}
Random Search uses the same hyper-parameter space of Grid Search, but replaces the \textit{brute-force search} with \textit{random search}. It is therefore possible that this approach will not find results as accurate as Grid Search, but it requires less computational time while finding reasonably good model in most cases. \cite{bergstra2012random}.

\subsection{Bayesian Optimization}
\label{bo}
Bayesian Optimization (BO) is a probabilistic model-based approach for optimizing objective functions ($f$) which are very expensive or slow to evaluate \cite{dewancker2015bayesian}. The key idea of this approach is to limit the evaluation phase of the objective function by spending more time to choosing the next set of hyper-parameters' values to try. 
In particular, this method is focused on solving the problem of finding the maximum of an expensive function $f: X \to \mathbb{R}$, 
\begin{equation}
arg max_ {x \in X}  f(x)
\end{equation}
where $X$ is the hyper-parameter space while can be seen as a hyper-cube where each dimension is a hyper-parameter.

BO builds a surrogate model of the objective function and quantifies the uncertainty in this surrogate using a regression model (e.g., \textit{Gaussian Process Regression}, see next section). Then it uses an acquisition function to decide where to sample the next set of hyper-parameter values \cite{frazier2018tutorial}.

The two main components of Bayesian Optimization are: the \textit{statistical model} (regression model) and the \textit{acquisition function} for deciding the next sampling. 

The statistical model provides a posterior probability distribution that describes potential values of the objective function at candidate points. As the number of observations grows, the posterior distribution improves and the algorithm becomes more certain of which regions in hyper-parameter space are worth to be explored and which are not. The acquisition function is used to find the point where the objective function is supposed to be maximal \cite{cosmetic}. This function defines a balance between \textit{Exploration} (new areas in the parameter space) and \textit{Exploitation} (picking values in areas that are already known to have favourable values); the aim of the \textit{Exploration} phase is to select samples that shrink the search space as much as possible, and the aim of the \textit{Exploitation} phase is to focus on the reduced search space and to select samples close to the optimum \cite{DBLP:journals/corr/abs-1204-0047}. 

Figure \ref{fig: GPr} shows the behaviour of this algorithm.

\subsubsection{Gaussian Process}
\label{GP}
A Gaussian Process (GP) is a stochastic process and a powerful prior distribution on functions. Any finite set of $N$ points ${\{ x_n \in X \}^{N}_{n=1}}$ induces a multivariate Gaussian distribution on $\mathbb{R}^{N}$. GP is completely determined by the mean function $m: X \to \mathbb{R}$ and the covariance function $K: X \times X \to \mathbb{R}$ \cite{rasmussen2003gaussian}.  For a complete and more precise overview on Gaussian Process and its application to Machine Learning, see \cite{rasmussen2003gaussian}.

\subsubsection{Acquisition function}
\label{af}
The acquisition function is used to decide where to sample the new set of hyper-parameters' values, Figure \ref{fig: af} shows. In the literature, there are many types of acquisition function. Usually, only three are used: \textit{Probability of Improvement}, \textit{Expected Improvement} and \textit{GP Upper Confidence Bound} (GP-UCB) \cite{cosmetic} \cite{snoek2012practical}.

\begin{figure}[pt]
	\centering
	\hspace*{-4.78cm}
	\makebox[\textwidth]{\includegraphics[scale=0.25]{./img/last.png}}
	\caption{BO behaviour. The figure shows the objective function (black line), mean (light purple line), covariance (purple halo) and the samplings (black dots). The top of the figure shows the Expected Improvement function (blue line) and his maximum (yellow dot). The next sampled point will be exactly the maximum of the activation function (red dot). The bottom of the figure shows  the new statistical model \protect\cite{wiki:gpbayesian}.}
	\label{fig: GPr}
\end{figure}

\begin{figure}[pt]
	\centering
	\hspace*{-4.7cm}
	\makebox[\textwidth]{\includegraphics[scale=0.45]{./img/acquisition_f.png}} 
	\caption{Example of Expected Improvement with the sampling of the next point (blue dot) generated with Scikit-Optimize library.}
	\label{fig: af}
\end{figure}

\FloatBarrier

The acquisition function used in this work is \textit{Expected Improvement} \cite{jones1998efficient}. The idea behind this acquisition function is to define a non-negative expected improvement over the best previously observed target value (previous best value) at a given point $x$. Expected Improvement is formalized as follows:
\begin{equation}
\label{EI}
EI_{y^*}(x) = \int_{-\infty}^{y^*} (y^* - y)p(y|x) \, dy
\end{equation}
where $y^*$ is the actual best result of the objective function, $x$ is the new proposed set of hyper-parameters' values, $y$ is the actual value of the objective function using $x$ as hyper-parameters' values and $p(y|x)$ is the surrogate probability model expressing the probability of $y$ given $x$.


\section{Network Behaviour Analysis}
\label{nba}
By automatically analysing the behaviour of the network, it is possible to identify problems that the DNNs could have after the training phase (e.g., overfitting, underfitting, etc).  Bayesian Optimization only works with a single metric (validation loss or accuracy, training loss or accuracy) and is not able to find problems like overfitting or fluctuating loss.

When these problems are diagnosed, the aim of the proposed algorithm is to shrink the hyper-parameters' value space or update the network architecture in order to drive the training to a better solution.

Many types of analyses can be performed on the network results, e.g., analysing the relation between training and validation loss or accuracy, the trend of accuracy or loss during the training phase in terms of direction and form of the trend.

For example: if a significant difference between the training loss and validation loss is found, and the validation loss is greater then the training loss, the diagnosis is that the network has a high variance and overfitting occurred, as shown in Figure \ref{fig: losses}.

\begin{figure}[pt]
	\centering
	\hspace*{-5.1cm}
	\makebox[\textwidth]{\includegraphics[scale=0.5]{./img/epoch_loss.png}} 
	\caption{The figure shows the difference between training loss (orange line) and evaluation loss (light blue line). This behaviour means that network incurred in overfitting.}
	\label{fig: losses}
\end{figure}


\subsection{Execution Pipeline}
\label{EP}
The software structure is composed by four main modules: \textit{training} module (NN in Figure \ref{fig: pipeline}), \textit{Diagnosis} module, \textit{Tuning Rules} module and the \textit{Search Space} driven by the \textit{controller} as can be seen in Figure \ref{fig: pipeline}. Controller rules the whole process. The main loop of the Algorithm \ref{alg: DNN-Tuner} uses the Controller for running the \textit{Diagnosis} module, the \textit{Tuning Rules} module and BO for performing the optimization.

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.15]{./img/pipeline2.png}} 
	\caption{The structure of the software with the main modules.}
	\label{fig: pipeline}
\end{figure}

Algorithm \ref{alg: DNN-Tuner} encapsulates the whole process (called \textit{DNN-Tuner}).

With $S$, $N$, $Params$ and $n$ we refer respectively to the hyper-parameters' value search space, initial neural network definition, a value picked from the search space and the number of cycles of the algorithm. $R$, $H$ and $M$ are the results of the evaluation of the trained network, the history of the loss and accuracy in both training and validation phases and the trained model of the network, respectively. $Ckpt$ is a checkpoint of the Bayesian Algorithm that can be used to restore the state of the Bayesian Optimization. $Issues$, $NewM$ and $NewsS$ are a list of issues found by the \textit{Diagnosis} module, the new model and the new restricted search space obtained from the \textit{Tuning} module, respectively. 

All the modules are implemented in Python. The DNN and BO are implemented with Keras and Scikit-Optimize respectively.

\FloatBarrier

\begin{algorithm}
	\caption{DNN-Tuner}
	\begin{algorithmic}
		\REQUIRE $S$, $N$, $n$
		\STATE $Pa rams \leftarrow Randomization(S)$
		\STATE $M \leftarrow Build Model(P,N)$
		\STATE $R,H \leftarrow Training(Params, M)$
		\STATE $Capt \leftarrow None$
		\WHILE{$n > 0$}
		\STATE $Issues \leftarrow Diagnosis(H, R)$
		\STATE $New M, News \leftarrow Tuning(Issues, M, S)$
		\IF{$New M \neq M$}
		\STATE $Params \leftarrow Randomization(News)$
		\STATE $R,H \leftarrow Training(Params, New M)$
		\ELSE
		\IF{$Ckpt$}
		\STATE $Ckpt \leftarrow BO(NewS,NewM, Ckpt)$
		\ENDIF
		\ENDIF
		\STATE $n \leftarrow (n-1)$
		\ENDWHILE
	\end{algorithmic}
	\label{alg: DNN-Tuner}
\end{algorithm}

\subsection{Diagnosis Module}
\label{PA}
In this work we have performed some proof-of-concept analyses. DNN-Tuner analyses the correlation between training and validation accuracy and loss in order to detect overfitting. It also analyses validation loss and accuracy in order to detect underfitting, and the trend of loss in order to fix the learning rate and the batch size.

For the first analysis, the proposed algorithm evaluates the difference of the accuracy in the training and validation phases in order to identify any gap as shown by Figure \ref{fig: overfitting}. An important gap between these two plots is a clear sign of overfitting, since this means that the network classifies well training data but not so well validation data, that is, it is unable to generalize. In this case, the algorithm can diagnose overfitting and then take action to correct this behaviour.

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.35]{./img/epoch_accuracy.png}\includegraphics[scale=0.35]{./img/epoch_loss_1.png}} 
	\caption{An example of accuracy (left) and loss (right) in the training (red line) and validation phase (blue line).}
	\label{fig: overfitting}
\end{figure}

For detecting underfitting, DNN-Tuner analyses the results of loss in both phases and estimates the amount of loss. If the loss is greater then $15\% \sim 16\%$, it diagnoses underfitting.

The analysis of the trend of the loss is useful for detecting the effects of the learning rate and batch size on the network. Given the trend, the proposed algorithm can check whether the loss is too noisy as in Figure \ref{fig: floating loss}, and whether the direction of the loss is descending, ascending or constant. In response to this diagnosis, the algorithm can attempt to set the batch size and learning rate correctly. 

These adjustments are performed by applying rules called \textit{Tuning Rules} (Section \ref{tr}).

\subsection{Tuning Rules}
\label{tr}
\textit{Tuning rules} are the actions applied to the network or to the hyper-parameters' value search space in response to the issues identified with the \textit{Diagnosis} module.

Tuning is applied following the IF-THEN paradigm as:
\FloatBarrier

\begin{algorithm}
	\caption{Tuning}
	\begin{algorithmic}
		\REQUIRE $Issues, M, S$
		\FOR{$I$ in $Issue$}
		\IF{$I = ``overfitting"$}
		\STATE $NewM, NewS \leftarrow Apply(T\-Rule\ 1, M, S)$
		\ELSIF{$I = ``underfitting"$}
		\STATE $NewM, NewS \leftarrow Apply(T\-Rule\ 2, M, S)$
		\ELSIF{$I = ``other \ issues" $}
		\STATE $NewM, NewS \leftarrow Apply(T\-Rule\ nth, M, S)$
		\ENDIF
		\ENDFOR
		\RETURN $NewM, NewS$
	\end{algorithmic}
	\label{alg: tuning}
\end{algorithm}

Then, for each issue found by the diagnosis module, a tuning rule is applied and a new search space and a new model are derived from the previous ones.

In the context of this work, the rules applied for each issue are shown Table \ref{table: rules}. 
\begin{table}[h!]
	\centering
	\begin{tabular}{| l | l |}
		\hline
		Issue & Tuning Rules\\ [1ex] 
		\hline
		Overfitting & Regularization \\ & Batch Normalization \\
		Underfitting & More neurons \\
		Fluctuating loss & Increase of batch size \\
		Increasing loss trend & Decrease of learning rate \\
		\hline
	\end{tabular}
	\caption{Rules applied on each issue.}
	\label{table: rules}
\end{table}


In order to prevent overfitting, the tuning module applies Batch Normalization \cite{DBLP:journals/corr/IoffeS15} and L2 regularization \cite{DBLP:journals/corr/Laarhoven17b}. For underfitting, it tries to increase the learning capacity of the network by removing small values from the space of the number of neurons in the convolutional and the fully connected layers. In this way, it drives the optimization to choose higher values for the number of neurons. If DNN-Tuner finds the trend of the loss to be fluctuating as in Figure \ref{fig: floating loss}, it force the optimization to choose larger batch size values. 

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.165]{./img/PRE_loss_1.png}} 
	\caption{In this figure we can see the fluctuating trend of the loss. May be due to a too small batch size.}
	\label{fig: floating loss}
\end{figure}

The amount of oscillation in the loss is related to the batch size. When the batch size is 1, the oscillation will be relatively high and the loss will be noisy. When the batch size is the complete dataset, the oscillation will be minimal because each update of the gradient should improve the loss function monotonically (unless the learning rate is set too high).

The last rule of Table \ref{table: rules} tries to fix a growing loss. When the loss grows or remains constant at too high values, this means that the learning rate is probably too high and the learning algorithm is unable to find the minimum because it takes too large steps. When the algorithm detects this behaviour, it removes large values from the learning rate search space.

\section{Experiments}
\label{ex}
We compare \textit{DNN-Tuner} with standard Bayesian Optimization implemented with library Scikit-Optimize. The tests were performed on CIFAR-10 and MNIST. Experiments were performed on a cluster with NVIDIA GPU K80 and 8-cores Intel Xeon E5-2630 v3.

The initial state of the DNN is composed by two blocks consisting of two Convolutional layers with ReLU as activation followed by a MaxPooling layer. At the end of second block there is a Dropout layer followed by a chain of two Fully Connected layers separated by a Dropout layer.
The initial hyper-parameters to be set by the algorithm are: the number of the neurons in the Convolutional layers, the values of the Dropout layers, the learning rate and the batch size.
Unlike Bayesian Optimization, our algorithm is able to update the network structure and the hyper-parameters to be set. For example, when our algorithm detects overfitting, it updates the network structure by adding L2 regularization at each Convolutional layers (kernel regularization), Batch Normalization after the activations and adds L2 regularization parameters to the hyper-parameters to be set.

Figures \ref{fig: acc-1} to \ref{fig: loss-mnist-7} show accuracy and loss in training and validation phases of both algorithms (DNN-Tuner and BO). Both algorithms have completed seven optimization cycles. The neural network has been trained for 200 epochs and the dataset used is CIFAR-10.

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/mio/epoch_accuracy_1.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/mio/epoch_loss_1.png}}
	\caption{Accuracy and loss in training (orange line) and validation (blue line) in the first step of DNN-Tuner.}
	\label{fig: acc-1}
\end{figure}
\begin{figure}[pt]
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/skopt/epoch_accuracy_1.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/skopt/epoch_loss_1.png}}
	\caption{Accuracy and loss in training (orange line) and validation (blue line) in the first step of Bayesian Optimization.}
	\label{fig: loss-1}
\end{figure}

Figures \ref{fig: acc-1} and \ref{fig: loss-1} show the difference in training and validation at the first cycle of optimization of DNN-Tuner and Bayesian Optimization respectively. You can observe that the trends are noisy because in the first cycle the hyper-parameters' values are chosen randomly. 

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/mio/epoch_accuracy_4.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.21]{./img/results/mio/epoch_loss_4.png}}
	\caption{Accuracy and loss in training (gray line) and validation (orange line) in the fourth step of DNN-Tuner.}
	\label{fig: acc-4}
\end{figure}
\begin{figure}[pt]
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/skopt/epoch_accuracy_4.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/skopt/epoch_loss_4.png}}
	\caption{Accuracy and loss in training (gray line) and validation (orange line) in the fourth step of Bayesian Optimization.}
	\label{fig: loss-4}
\end{figure}

Figures \ref{fig: acc-4} and \ref{fig: loss-4} show the difference in training and validation at the fourth cycle of optimization of DNN-Tuner and Bayesian Optimization respectively. You can observe that the trends of BO continue to be noisy, while the DNN-Tuner trends become less fluctuating with an accuracy of $\sim0,75$ and a loss of $\sim0,8$ on the validation set.

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/mio/epoch_accuracy_7.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/mio/epoch_loss_7.png}}
	\caption{Accuracy and loss in training (green line) and validation (gray line) in the seventh step of DNN-Tuner.}
	\label{fig: acc-7}
\end{figure}
\begin{figure}[pt]
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/skopt/epoch_accuracy_7.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.23]{./img/results/skopt/epoch_loss_7.png}}
	\caption{Accuracy and loss in training (green line) and validation (gray line) in the seventh step of Bayesian Optimization.}
	\label{fig: loss-7}
\end{figure}

Figures \ref{fig: acc-7} and \ref{fig: loss-7} show the difference in training and validation at the seventh and last cycle of optimization of DNN-Tuner and Bayesian Optimization respectively. You can observe that the trends of BO still continues to be noisy, while the DNN-Tuner trends reaches an accuracy of $\sim0,83$ and a loss of $\sim2$ on the validation set.

Finally, Figures \ref{fig: acc-mnist-7} and \ref{fig: loss-mnist-7} show the difference of training and validation at the seventh and last cycle of optimization of DNN-Tuner and Bayesian Optimization on the MNIST dataset, also with 200 training epochs. We can see that DNN-Tuner, reaches an accuracy $\sim0,993$ and a loss of $\sim0,04$ in the validation set unlike BO which reaches $\sim0,114$ and $\sim2,3$.

In terms of time, for the seven cycles of optimization with 200 training epochs on MNIST dataset: DNN-Tuner employed 4,75 hours while Bayesian Optimization 5,74 hours.

\section{Conclusion and Future Work}
\label{conclusion}
DNNs have achieved extraordinary results but the amount of hyper-parameters to be set has led to new challenges in the field of Artificial Intelligence. 
Given this problem, and the ever larger size of the networks, the standard approaches of hyper-parameters tuning like Grid and Random Search are no longer able to provide satisfactory results in acceptable time.
Inspired by the Bayesian Optimization (BO) technique, we have inserted expert knowledge into the tuning process, comparing our algorithm with BO in terms of quality of the results and time.
The experiments show that it is possible to combine these approaches to obtain a better optimization of DNNs. The new algorithm implements tuning rules that work on the network structure and on the  hyper-parameter' search space to drive the training of the network towards better results.  Some experiments were performed over standard datasets to demonstrate the improvement provided by DNN-Tuner.
Future works will be focused on real-time networks analysis for performance speed-up and on the implementation of the tuning rules with symbolic languages.

\begin{figure}[pt]
	\centering
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.30]{./img/results/mio/epoch_accuracy-keras-7.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.30]{./img/results/mio/epoch_loss-keras-7.png}}
	\caption{Accuracy and loss in training (light blue line) and validation (purple line) of the seventh step of DNN-Tuner.}
	\label{fig: acc-mnist-7}
\end{figure}
\begin{figure}[pt]
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.30]{./img/results/skopt/epoch_accuracy-keras-7-1.png}}
	\hspace*{-4.8cm}
	\makebox[\textwidth]{\includegraphics[scale=0.30]{./img/results/skopt/epoch_loss-keras-7-1.png}}
	\caption{Accuracy and loss in training (light blue line) and validation (purple line) of the seventh step of Bayesian Optimization.}
	\label{fig: loss-mnist-7}
\end{figure}


\FloatBarrier
\bibliographystyle{named}
\bibliography{ijcai20}

\end{document}

